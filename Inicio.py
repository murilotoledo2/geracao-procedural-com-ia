# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 18:45:02 2024

@author: MURILO TOLEDO
"""

import pygame
import os
import random

# Configurações do mapa
largura_mapa = 800
altura_mapa = 600
tamanho_bloco = 40
altura_maxima_verde = 3
probabilidade_caixa = 0.05
probabilidade_agua = 0.2  # Aumentando a probabilidade de água
largura_maxima_agua = 7  # Largura máxima do bloco azul
minimo_blocos_agua = 2  # Número mínimo de blocos azuis

# Inicia o PyGame
pygame.init()

# Criar Janela com PyGame
janela = pygame.display.set_mode((largura_mapa, altura_mapa))
pygame.display.set_caption("Gerador de Mapas 2D")

# Diretório do script
diretorio_script = os.path.dirname(os.path.abspath(__file__))

# Carregar Texturas
textura_verde = pygame.image.load(os.path.join(diretorio_script, "Texturas/textura_grama.png"))
textura_amarela = pygame.image.load(os.path.join(diretorio_script, "Texturas/textura_caixa.png"))
textura_azul = pygame.image.load(os.path.join(diretorio_script, "Texturas/textura_agua.png"))
textura_ceu = pygame.image.load(os.path.join(diretorio_script, "Texturas/textura_ceu1.png"))

# Função para desenhar o mapa com texturas
def desenhar_mapa(mapa):
    for y, linha in enumerate(mapa):
        for x, bloco in enumerate(linha):
            textura = texturas[bloco]
            janela.blit(textura, (x * tamanho_bloco, y * tamanho_bloco))

# Função para gerar um mapa procedural
def gerar_mapa(largura, altura):
    mapa = [[3] * largura for _ in range(altura)]

    for x in range(largura):
        altura_verde = random.randint(1, altura_maxima_verde)
        for y in range(altura - altura_verde, altura):
            mapa[y][x] = 0

        if random.random() < probabilidade_caixa:
            y_caixa = altura - altura_verde - 4
            if y_caixa >= 0:
                mapa[y_caixa][x] = 1

    # Geração do bloco azul escuro
    contagem_agua = 0
    while contagem_agua < minimo_blocos_agua:
        largura_agua = random.randint(1, largura_maxima_agua)
        x_inicio = random.randint(0, largura - largura_agua)
        x_fim = x_inicio + largura_agua

        # Verificar se a área é válida para o bloco azul
        if (
            all(mapa[y][x_inicio:x_fim] != 0 for y in range(altura))  # Verificar se não há bloco verde dentro da largura do bloco azul
            and all(mapa[y][x] != 0 for y in range(altura - 1) for x in range(x_inicio, x_fim))  # Verifica se não há bloco verde acima do bloco azul
        ):
            for x in range(x_inicio, x_fim):
                mapa[altura - 1][x] = 2
            contagem_agua += 1

    return mapa

# Mapeamento de texturas
texturas = {
    0: textura_verde,
    1: textura_amarela,
    2: textura_azul,
    3: textura_ceu,
}

# Geração inicial do mapa
mapa = gerar_mapa(largura_mapa // tamanho_bloco, altura_mapa // tamanho_bloco)


def salvar_mapa_como_imagem(mapa, nome_arquivo):
    surface = pygame.Surface((largura_mapa, altura_mapa))
    desenhar_mapa(mapa)
    pygame.image.save(surface, nome_arquivo)


#geracao=0

tam=11
i=1
while i < tam :
    
    #Linha abaixo para gerar o mapa
    mapa = gerar_mapa(largura_mapa // tamanho_bloco, altura_mapa // tamanho_bloco)
    
    # Desenha o mapa com texturas
    janela.fill(textura_ceu.get_at((0, 0)))  # Preenche o fundo com a cor do céu (pegando a cor do canto superior esquerdo da textura)
    desenhar_mapa(mapa)
    
    nome_arquivo = f"Mapas/geracao_{i}.png"    
    pygame.image.save(janela, nome_arquivo)
    
    i=i+1
    
    

# Finaliza o Pygame
pygame.quit()
