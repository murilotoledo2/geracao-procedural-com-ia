# -*- coding: utf-8 -*-
"""
Created on Thu May 30 15:46:27 2024

@author: Murilo Toledo
"""

import numpy as np
import os
import shutil
import tensorflow as tf
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.applications.resnet50 import preprocess_input
from sklearn.metrics.pairwise import cosine_similarity

def carregar_imagens(pasta_imagem):
    images = []
    for filename in os.listdir(pasta_imagem):
        if filename.endswith(".png") or filename.endswith(".jpg"):
            image_path = os.path.join(pasta_imagem, filename)
            img = image.load_img(image_path, target_size=(224, 224))
            img = image.img_to_array(img)
            img = preprocess_input(img)
            images.append((image_path, img))
    return images

def procurar_imagem_similar(local_da_imagem_alvo, pasta_imagem):
    # Com o TensorFlow foi utilizado o modelo ResNet50 que é pré treinado
    model = ResNet50(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
    
    # Carregando as imagens
    imagem_alvo = image.load_img(local_da_imagem_alvo, target_size=(224, 224))
    imagem_alvo = image.img_to_array(imagem_alvo)
    imagem_alvo = preprocess_input(imagem_alvo)
    imagem_alvo = np.expand_dims(imagem_alvo, axis=0)

    images = carregar_imagens(pasta_imagem)
    image_paths, image_arrays = zip(*images)
    
    # Obtendo os embeddings das imagens usando o modelo ResNet50
    #Usa um modelo ResNet50 pré-treinado para obter embeddings (representações vetoriais) de uma imagem alvo e de um conjunto de imagens.
    #Converte esses embeddings em vetores unidimensionais para a imagem alvo e em uma matriz bidimensional para o conjunto de imagens.
    target_embedding = model.predict(imagem_alvo).flatten()
    image_embeddings = model.predict(np.array(image_arrays)).reshape(len(image_arrays), -1)
    
    # Calculando a similaridade de cosseno entre a imagem alvo e todas as outras imagens
    # Para medir suas semelhanças.
    similaridades = cosine_similarity(target_embedding.reshape(1, -1), image_embeddings)
    
    # Encontrando a imagem mais similar
    indice_mais_similar = np.argmax(similaridades)
    caminho_imagem_mais_similar = image_paths[indice_mais_similar]
    
    return caminho_imagem_mais_similar

# Exemplo de uso
local_da_imagem_alvo = "C:/Users/muril/Desktop/TCC/V1/Escolha/Dificil/base.png"
pasta_imagem = "C:/Users/muril/Desktop/TCC/V1/Mapas"

local_da_imagem_similar = procurar_imagem_similar(local_da_imagem_alvo, pasta_imagem)
print("A imagem mais similar é:", local_da_imagem_similar)

#Salvando imagem escolhida

# Caminhos e nomes de arquivo
localorigem = local_da_imagem_similar
pastadestino = "C:/Users/muril/Desktop/TCC/V1/Escolha/Dificil"
novonome_arquivo = "escolhida.png"

# Criando o diretório de destino, se não existir
os.makedirs(pastadestino, exist_ok=True)

# Construindo o caminho de destino
localdestino = os.path.join(pastadestino, novonome_arquivo)

# Copiando e salvando a imagem com um novo nome no novo diretório
shutil.copy(localorigem, localdestino)

